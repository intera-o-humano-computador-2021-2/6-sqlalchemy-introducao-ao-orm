from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.automap import automap_base

from datetime import timedelta

# ----- VARIÁVEIS GLOBAIS ------------------------------------
# Ligação com o esquema de banco de dados
engine = create_engine(
    "mysql+mysqlconnector://root:hiragi7@db/db_work?charset=utf8mb4")

# Mapeamento Objeto Relacional com o SQLAlchemy
DB = automap_base()
DB.prepare(engine, reflect=True)
tb_servico = DB.classes.tb_servico
tb_cliente = DB.classes.tb_cliente
ta_agendamento = DB.classes.ta_agendamento

# Trabalho com sessões da base agora Objeto-Relacional
session_factory = sessionmaker(bind=engine)
ses = session_factory()
# ------------------------------------------------------------

# # Listar todos os serviços
# servicos = ses.query(tb_servico).all()

# for s in servicos:
#     print(
#         s.dsc_servico,
#         s.vlr_servico,
#         s.tb_servico
#     )
# # ------------------------------------------------------------

# Listar os agendamentos por cliente
servicos = ses.query(tb_servico).all()

for s in servicos:
    print('-' * 50)
    print('Descrição:', s.dsc_servico)
    print('Valor:', s.vlr_servico)
    print('Tempo:', s.tmp_servico)
    n = 1
    for a in s.ta_agendamento_collection:
        print('Agendamento-{}:'.format(n), 'de', a.dti_agendamento, 'às', a.dti_agendamento + timedelta(
            hours=a.tb_servico.tmp_servico), a.tb_servico.dsc_servico, '(R$ {})'.format(a.tb_servico.vlr_servico))
        n += 1
# ------------------------------------------------------------
