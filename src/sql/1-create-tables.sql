/* Service Table */
DROP TABLE IF EXISTS tb_servico;

CREATE TABLE tb_servico(
  idt_servico INT AUTO_INCREMENT,
  dsc_servico TEXT,
  vlr_servico DECIMAL(8,2),
  tmp_servico INT,
  PRIMARY KEY (idt_servico)
);
/* ------------------------------------------------------- */

/* Client Table */
DROP TABLE IF EXISTS tb_cliente;

CREATE TABLE tb_cliente(
  idt_cliente INT AUTO_INCREMENT,
  nme_cliente VARCHAR(255),
  end_cliente TEXT,
  tel_cliente VARCHAR(20),
  PRIMARY KEY (idt_cliente)
);
/* ------------------------------------------------------- */

/* Scheduling table */
DROP TABLE IF EXISTS ta_agendamento;

CREATE TABLE ta_agendamento(
  idt_agendamento INT AUTO_INCREMENT,
  cod_servico INT,
  cod_cliente INT,
  dti_agendamento DATETIME,
  PRIMARY KEY (idt_agendamento),
  FOREIGN KEY (cod_servico) REFERENCES tb_servico(idt_servico),
  FOREIGN KEY (cod_cliente) REFERENCES tb_cliente(idt_cliente)
 );
/* ------------------------------------------------------- */
